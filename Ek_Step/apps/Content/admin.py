from django.contrib import admin
from .models import Content
from import_export.admin import ImportExportModelAdmin

# Register your models here.
@admin.register(Content)
class ContentAdmin(ImportExportModelAdmin):
    pass