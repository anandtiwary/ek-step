from django.db import models
from apps.configuration.models import Book, Grade, State, Medium, Subject
from django.contrib.auth.models import User
from apps.Table_of_content.models import TOC
from apps.Hard_Spot.models import HardSpot
from django.core.validators import MaxValueValidator
# Create your models here.
class Content(models.Model):

	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)
	email = models.EmailField()
	mobile = models.BigIntegerField(blank=False,
		null=False)
	hard_spot = models.ForeignKey(HardSpot,
		null=True,
		blank=True,
		on_delete=models.PROTECT)
	toc = models.ForeignKey(TOC,
		on_delete=models.PROTECT)
	content_name = models.CharField(max_length=200)
	video_link = models.URLField()
	approved = models.BooleanField(default=False)
	approved_by = models.ForeignKey(User,
		on_delete=models.PROTECT,
		related_name='%(class)s_approved_by')
	rating = models.PositiveIntegerField(validators=[MaxValueValidator(5)],
		blank=True)
	rated_by = models.ForeignKey(User,
		on_delete=models.PROTECT,
		related_name='%(class)s_rated_by')

	def __str__(self):
		return self.content_name

	class Meta:
		verbose_name='content'