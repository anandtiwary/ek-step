from django.contrib import admin
from .models import Book, Grade, State, Medium, Subject
from import_export.admin import ImportExportModelAdmin

# Register your models here.
# models = [Book, Grade, State, Medium, Subject]
# admin.site.register(models)

@admin.register(Book,Grade,State,Medium,Subject)
class ConfigAdmin(ImportExportModelAdmin):
    pass