from django.apps import AppConfig


class HardSpotConfig(AppConfig):
    name = 'apps.Hard_Spot'
    verbose_name = 'Hard Spot'