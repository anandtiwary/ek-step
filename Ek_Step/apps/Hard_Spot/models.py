from django.db import models
from apps.configuration.models import Book, Grade, State, Medium, Subject
from django.contrib.auth.models import User
from apps.Table_of_content.models import TOC
from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator
# Create your models here.
class HardSpot(models.Model):

	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)
	email = models.EmailField()
	mobile =models.CharField(max_length=10,
		blank=False,
		null=False)
	toc = models.ForeignKey(TOC,
		on_delete=models.PROTECT)

	hard_spot = models.CharField(max_length=200)
	description = models.TextField()
	impact = models.TextField()
	approval = models.BooleanField(default=False)
	approved_by = models.ForeignKey(User,
		on_delete=models.PROTECT)

	def __str__(self):
		return self.hard_spot

	class Meta:
		verbose_name='Hard Spot'
		verbose_name_plural='Hard Spots'