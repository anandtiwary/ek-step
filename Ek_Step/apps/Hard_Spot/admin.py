from django.contrib import admin
from .models import HardSpot
from import_export.admin import ImportExportModelAdmin

# Register your models here.

@admin.register(HardSpot)
class HardSpotAdmin(ImportExportModelAdmin):
    pass