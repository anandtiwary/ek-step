from django.contrib import admin
from .models import TOC
from import_export.admin import ImportExportModelAdmin

# Register your models here.
# <<<<<<< Updated upstream
# class PersonAdmin(admin.ModelAdmin):
#     list_filter = ('state__state', 'medium__medium', 'grade__grade', 'subject', 'book__book')

# admin.site.register(TOC, PersonAdmin)
# =======

# @admin.register(TOC)
class TocAdmin(ImportExportModelAdmin):
    list_filter = ('state__state', 'medium__medium', 'grade__grade', 'subject', 'book__book')
# >>>>>>> Stashed changes
admin.site.register(TOC, TocAdmin)