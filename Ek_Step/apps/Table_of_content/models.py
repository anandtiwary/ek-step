from django.db import models
from apps.configuration.models import Book, Grade, State, Medium, Subject
from django.contrib.auth.models import User
# Create your models here.

class Chapter(models.Model):
	book = models.ForeignKey(Book,
		on_delete=models.PROTECT)
	chapter = models.CharField(max_length=200)

	def __str__(self):
		return self.chapter

	class Meta:
		verbose_name='Chapter'
		verbose_name_plural='Chapter'

class Section(models.Model):
	chapter = models.ForeignKey(Chapter,
		on_delete=models.PROTECT)
	section = models.CharField(max_length=200)

	def __str__(self):
		return self.section

	class Meta:
		verbose_name='Section'
		verbose_name_plural='Sections'

class SubSection(models.Model):
	section = models.ForeignKey(Section,
		on_delete=models.PROTECT)
	sub_section = models.CharField(max_length=200)

	def __str__(self):
		return self.sub_section

	class Meta:
		verbose_name='Sub section'
		verbose_name_plural='Sub sections'

class TOC(models.Model):
	state = models.ForeignKey(State,
		on_delete=models.PROTECT)
	medium = models.ForeignKey(Medium,
		on_delete=models.PROTECT)
	grade = models.ForeignKey(Grade,
		on_delete=models.PROTECT)
	subject = models.ForeignKey(Subject,
		on_delete=models.PROTECT)
	book = models.ForeignKey(Book,
		on_delete=models.PROTECT)
	chapter = models.ForeignKey(Chapter,
		on_delete=models.PROTECT)
	section = models.ManyToManyField(Section,
		blank=True,
		max_length=200)
	sub_section = models.ManyToManyField(SubSection,
		blank=True,
		max_length=200)
	approved = models.BooleanField(default=False)
	approved_by	= models.ForeignKey(User, 
		null=True,
		related_name='%(class)s_approved_by',
		on_delete=models.PROTECT)
	created_by = models.ForeignKey(User, 
		null=True,
		related_name='%(class)s_created_by',
		on_delete=models.PROTECT)

	def __str__(self):
		return self.chapter + "-" + self.section + "-" + self.sub_section

	class Meta:
		verbose_name='Table of content'
		verbose_name_plural='Table of Content'