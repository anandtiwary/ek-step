from django.apps import AppConfig


class TableOfContentConfig(AppConfig):
    name = 'apps.Table_of_content'
    verbose_name='Table of Content'
